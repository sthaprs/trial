Rails.application.routes.draw do
	resources :books
  resources :publication_houses
  resources :categories
  resources :authors
  resources :book_sales
	root to: 'books#index'
end


