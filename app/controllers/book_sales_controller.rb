class BookSalesController < ApplicationController
before_action :find_book_sale, only: %i(show)  
  def index
    @book_sales = BookSale.all
    @book = Book.all
  end

  def new
    @book_sale = BookSale.new
    @books = Book.all
  end

  def create
      @book_sale = BookSale.new(book_sale_params)
    if @book_sale.save
      redirect_to book_sales_path
    else
      redirect_to new_book_sale_path
    end
  end

  def show
    
  end

  private

  def book_sale_params
    params.require(:book_sale).permit(:cust_name, :quantity, :cust_address, :book_id)
  end

  def permitted_params
    params.permit(:id)
  end

  def find_book_sale
    @book_sale = BookSale.find permitted_params['id']  
  end
end
