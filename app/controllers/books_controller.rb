class BooksController < ApplicationController
	before_action :find_book, only: %i[show edit update destroy]

  def index
		@books = Book.all
	end

  def new
    @book = Book.new
    @authors = Author.all
    @categories = Category.all
  end

  def create
    @book = Book.new(book_params)
    if @book.save
      redirect_to books_path
    else
      @authors = Author.all
      redirect_to new_book_path
    end
  end

  def edit
    @authors = Author.all
    @categories = Category.all
  end

  def update
    if @book.update(book_params)
      redirect_to books_path
    else
      redirect_to edit_book_path
    end
  end

  def show
  end

  def destroy
    if @book.destroy
      redirect_to books_path
    else
      redirect_to book_path
    end

  end

  private

  def book_params
    params.require(:book).permit(:name, :publication_date, :price, :author_id, :description, :category_id)
  end

  def permitted_params
    params.permit(:id)
  end

  def find_book
    @book = Book.find permitted_params['id']
    
  end
end
