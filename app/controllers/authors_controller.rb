class AuthorsController < ApplicationController
  before_action :find_author, only: %i[show edit update destroy]

  def index
    @authors = Author.all    
  end

  def new
    @author = Author.new
    @publication_houses = PublicationHouse.all
  end

  def create
    @author = Author.new(author_params)
    if @author.save
      redirect_to authors_path
    else
      flash[:error] = @author.errors.full_messages.join(", ")
      redirect_to new_author_path
    end
  end

  def edit
    @publication_houses = PublicationHouse.all
  end

  def update
    if @author.update(author_params)
      redirect_to authors_path
    else
      @publication_houses = PublicationHouse.all
      render :edit
    end
  end

  def show
  end

  def destroy
    if @author.destroy
      redirect_to authors_path
    else
      redirect_to author_path
    end
  end

  private

  def author_params
    params.require(:author).permit(:name, :address, :pen_name, :dob, :phone, :publication_house_id)
  end

  def permitted_params
    params.permit(:id)
  end

  def find_author
    @author = Author.find permitted_params['id']
  end
end
