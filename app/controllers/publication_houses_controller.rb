class PublicationHousesController < ApplicationController
  before_action :find_publication_house, only: %i[show destroy]
  def index
    @publication_houses = PublicationHouse.all    
  end

  def new
    @publication_house = PublicationHouse.new
  end

  def create
  
    @publication_house = PublicationHouse.new(publication_house_params)
    if(@publication_house.save)
      redirect_to publication_houses_path
    else
      render:new
    end
  end

  def edit
    find_publication_house
  end

  def update
    find_publication_house
    if @publication_house.update(publication_house_params)
      redirect_to publication_house_path
    else
      render :edit
    end
  end
  
  def show
    
  end

  def destroy
    if @publication_house.destroy
      redirect_to publication_houses_path
    else
      redirect_to publication_house_path
    end
    
  end
  private
  def publication_house_params
    params.require(:publication_house).permit(:name, :address, :reg_no, :phone)
  end
  
  def permitted_params
    params.permit(:id)
  end
  def find_publication_house
    @publication_house = PublicationHouse.find permitted_params['id'] 
  end
end
