class Book < ApplicationRecord
  validates :name, :publication_date, presence: true
  validates :price, numericality: true
  belongs_to :author
  belongs_to :category
  has_many :book_sales
end
