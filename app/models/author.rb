class Author < ApplicationRecord
  validates :name, presence: true
  has_many :books
  belongs_to :publication_house
end
