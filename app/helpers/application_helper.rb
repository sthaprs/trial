module ApplicationHelper
  def custom_bootstrap_flash
    flash_messages = []
    flash.each do |type, message|
      text = "toastr.#{type}(`#{message}`);"
      flash_messages << text.html_safe if message
    end
    flash_messages = flash_messages.join('\n')
    "<script>#{ flash_messages.to_s.html_safe }</script>".html_safe
  end
end
