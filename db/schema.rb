# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_24_035950) do

  create_table "authors", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "pen_name"
    t.datetime "dob"
    t.string "phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "publication_house_id"
    t.index ["publication_house_id"], name: "index_authors_on_publication_house_id"
  end

  create_table "book_sales", force: :cascade do |t|
    t.string "cust_name"
    t.integer "quantity"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "book_id"
    t.string "cust_address"
    t.index ["book_id"], name: "index_book_sales_on_book_id"
  end

  create_table "books", force: :cascade do |t|
    t.string "name"
    t.string "publication"
    t.datetime "publication_date"
    t.float "price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "description"
    t.integer "author_id"
    t.integer "category_id"
    t.index ["author_id"], name: "index_books_on_author_id"
    t.index ["category_id"], name: "index_books_on_category_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "cat_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "publication_houses", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "reg_no"
    t.string "phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "authors", "publication_houses"
  add_foreign_key "book_sales", "books"
  add_foreign_key "books", "authors"
  add_foreign_key "books", "categories"
end
