class RenameBooksaleNameToCustName < ActiveRecord::Migration[6.0]
  def change
    rename_column :book_sales, :name, :cust_name
  end
end
