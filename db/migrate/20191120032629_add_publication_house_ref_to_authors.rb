class AddPublicationHouseRefToAuthors < ActiveRecord::Migration[6.0]
  def change
    add_reference :authors, :publication_house, foreign_key: true
  end
end
