class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.string :name
      t.string :publication
      t.datetime :publication_date
      t.float :price

      t.timestamps
    end
  end
end
