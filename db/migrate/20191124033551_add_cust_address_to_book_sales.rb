class AddCustAddressToBookSales < ActiveRecord::Migration[6.0]
  def change
    add_column :book_sales, :cust_address, :string
  end
end
