class AddBookRefToBookSales < ActiveRecord::Migration[6.0]
  def change
    add_reference :book_sales, :book, foreign_key: true
  end
end
