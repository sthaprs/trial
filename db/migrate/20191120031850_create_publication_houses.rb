class CreatePublicationHouses < ActiveRecord::Migration[6.0]
  def change
    create_table :publication_houses do |t|
      t.string :name
      t.string :address
      t.string :reg_no
      t.string :phone

      t.timestamps
    end
  end
end
